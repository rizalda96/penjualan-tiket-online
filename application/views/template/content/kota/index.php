<div class="container mt-5">
	<!-- <button data-toggle="modal" data-target="#addModal" class="btn btn-success mb-2">Tambah Data</button> -->
	<button type="button" class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#addModal">
		Tambah Data Kota
	</button>

	<table id="example" class="table table-striped" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kota</th>
				<th>Status Aktif</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody id="tbl_data">
		</tbody>
	</table>
</div>

<!-- tambah modal -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="nama_kota">Nama Kota</label>
						<input type="text" name="nama_kota" class="form-control"></input>
					</div>
					<div class="form-group">
						<!-- <label for="status_aktif">Status Aktif</label> -->
						<input type="hidden" name="status_aktif" value="1" class="form-control" readonly></input>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- end tambah modal -->

<!-- Modal Edit-->
<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="nama_kota">Nama Kota</label>
						<input type="hidden" name="id" class="form-control"></input>
						<input type="text" name="nama_kota_edit" class="form-control"></input>
					</div>
					<div class="form-group">
						<label for="status_aktif">Status Aktif</label>
						<input type="number" name="status_aktif" class="form-control"></input>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="btn_update_data">Simpan</button>
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" />

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	// $(document).ready(function() {
		
		var example = $('#example').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "<?= base_url() ?>kota/list",
				"type": "post",
			},

			// ambil data dari field
			"columns": [{
					data: "id"
				},
				{
					data: "nama_kota"
				},
				{
					data: "status_aktif"
				},
				{
					data: "id",
					render: function(data, type, full, meta) {
						// console.log(data);
						var str = '';
						str += '<button class="btn btn-primary btn-xs btn_edit" data-id= "' + data + '"> Edit </button>&nbsp';
						str += '<button class="btn btn-danger btn-xs btn_hapus" data-id= "' + data + '"> Delete </button>';
						return str;
					}
				},

			]
		});

		// hapus
		$("#tbl_data").on('click', '.btn_hapus', function() {
			var id = $(this).attr('data-id');
			var status = confirm('Yakin ingin menghapus?');
			if (status) {
				$.ajax({
					url: '<?php echo base_url(); ?>kota/hapusData',
					type: 'POST',
					data: {
						id: id
					},
					success: function(response) {
						example.ajax.reload();
					}
				})
			}
		})

		// add
		$("#btn_add_data").on('click', function() {
			var nama_kota = $('input[name="nama_kota"]').val();
			var status_aktif = $('input[name="status_aktif"]').val();
			$.ajax({
				url: '<?php echo base_url(); ?>kota/tambahData',
				type: 'POST',
				data: {
					nama_kota: nama_kota,
					status_aktif: status_aktif
				},
				success: function(response) {
					$('input[name="nama_kota"]').val("");
					$("#addModal").modal('hide');
					example.ajax.reload();
				}
			})
		});

		//Memunculkan modal edit
		$("#tbl_data").on('click', '.btn_edit', function() {
			var id = $(this).attr('data-id');
			$.ajax({
				url: '<?php echo base_url(); ?>kota/getDataById',
				type: 'POST',
				data: {
					id: id
				},
				dataType: 'json',
				success: function(response) {
					$("#editModal").modal('show');
					$('input[name="id"]').val(response.id);
					$('input[name="nama_kota_edit"]').val(response.nama_kota);
					$('input[name="status_aktif"]').val(response.status_aktif);
				}
			})
		});


		//Meng-Update Data
		$("#btn_update_data").on('click', function() {
			var id = $('input[name="id"]').val();
			var nama_kota = $('input[name="nama_kota_edit"]').val();
			var status_aktif = $('input[name="status_aktif"]').val();
			$.ajax({
				url: '<?php echo base_url(); ?>kota/perbaruiData',
				type: 'POST',
				data: {
					id: id,
					nama_kota_edit: nama_kota,
					status_aktif: status_aktif
				},
				success: function(response) {
					// $('input[name="id"]').val("");
					// $('input[name="nama_kota"]').val("");
					// $('input[name="status_aktif"]').val("");
					$("#editModal").modal('hide');
					example.ajax.reload();
				}
			})

		});


	// });
</script>
