<div class="container mt-5">
	<!-- <button data-toggle="modal" data-target="#addModal" class="btn btn-success mb-2">Tambah Data</button> -->
	<!-- <button type="button" class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#addModal">
		Tambah Data Kota
	</button> -->

	<table id="tbl-order" class="table table-striped" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th>Kota Asal</th>
				<th>Kota Tujuan</th>
				<th>Tanggal Berangkat</th>
				<th>Seat Nomor</th>
				<th>Harga</th>
			</tr>
		</thead>
		<tbody id="tbl-data-order">
		</tbody>
	</table>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" />

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#tbl-order').DataTable({
			"serverSide": true,
			"ajax": {
				"url": "<?= base_url() ?>order/list",
				"type": "post",
				"success": function(response) {
					var i;
					var no = 0;
					var html = "";
					for (i = 0; i < response.length; i++) {
						no++;
						html = html + '<tr>' +
							'<td>' + no + '</td>' +
							'<td>' + response[i].asal + '</td>' +
							'<td>' + response[i].tujuan + '</td>' +
							'<td>' + response[i].tanggal + '</td>' +
							'<td>' + response[i].seat + '</td>' +
							'<td>' + response[i].harga + '</td>' +
							'</tr>';
					}
					$("#tbl-data-order").html(html);
				}
			},
		});
	});
</script>
