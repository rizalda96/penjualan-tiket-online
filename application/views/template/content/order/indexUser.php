<div class="container mt-5">
	<h5>
		Pesan Tiket
	</h5>
	<form>
		<div class="form-group">
			<label for="asal">Kota Asal</label>
			<select id="asal" name="asal" class="form-control select2_asal" required>
				<option></option>
			</select>
		</div>
		<div class="form-group">
			<label for="tujuan">Kota Tujuan</label>
			<select id="tujuan" name="tujuan" class="form-control select2_tujuan" required>
				<option></option>
			</select>
		</div>
		<div class="form-group">
			<label for="tanggal">Tanggal Berangkat</label>
			<input type="date" name="tanggal" class="form-control"></input>
		</div>
		<div class="form-group">
			<label for="seat">Seat Nomor</label>
			<select id="seat" class="form-select" name="seat" aria-label="Default select example">
				<option selected>Pilih Seat</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
			</select>
		</div>
		<div class="form-group">
			<label for="harga">Harga</label>
			<input type="text" name="harga" class="form-control" id="harga" readonly></input>
		</div>
		<button type="button" class="btn btn-success" id="btn_order_tiket">Beli Tiket</button>
	</form>
</div>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css" />

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>
	$('.select2_tujuan').select2({
		placeholder: "Pilih Kota Tujuan",
		theme: 'bootstrap4',
		ajax: {
			dataType: 'json',
			delay: 250,
			url: '<?php echo base_url(''); ?>kota/listKota',
			data: function(params) {
				return {
					search: params.term
				}
			},
			processResults: function(data) {
				return {
					results: $.map(data, function(obj) {
						return {
							id: obj.id,
							text: obj.nama_kota
						};
					})
				};
			}
		}
	}).on('change', function(e) {
		var tujuan = $('.select2_tujuan').val();
		if (tujuan == 1) {
			document.getElementById("harga").value = '30000';
		}
		if (tujuan == 4) {
			document.getElementById("harga").value = '50000';
		}
		if (tujuan == 6) {
			document.getElementById("harga").value = '60000';
		}
		if (tujuan == 12) {
			document.getElementById("harga").value = '120000';
		}
	});

	$('.select2_asal').select2({
		placeholder: "Pilih Kota Asal",
		theme: 'bootstrap4',
		ajax: {
			dataType: 'json',
			delay: 250,
			url: '<?php echo base_url(''); ?>kota/listKota',
			data: function(params) {
				return {
					search: params.term
				}
			},
			processResults: function(data) {
				return {
					results: $.map(data, function(obj) {
						return {
							id: obj.id,
							text: obj.nama_kota
						};
					})
				};
			}
		}
	});

	$("#btn_order_tiket").on('click', function() {
		var tujuan = $('.select2_tujuan').val();
		var asal = $('.select2_asal').val();
		var tanggal = $('input[name="tanggal"]').val();
		// var seat = $("#seat").val();
		var seat = $("#seat option:selected").val();
		var harga = $('input[name="harga"]').val();

		$.ajax({
			url: '<?php echo base_url(); ?>order/bayar',
			type: 'POST',
			data: {
				asal: asal,
				tujuan: tujuan,
				tanggal: tanggal,
				seat: seat,
				harga: harga,
			},
			success: function(response) {
				$('.select2_tujuan').val("");
				$('.select2_asal').val("");
				$('input[name="tanggal"]').val("");
				$('input[name="harga"]').val("");
				// $('#seat').text("");

				window.location.reload();
			}
		})
	});
</script>
