<div class="container mt-5">
	<div class="body">
		<form id="sign_up" method="POST" action="<?php echo base_url(); ?>login/daftar">
			<div class="mb-4">Register a new membership</div>
			<div class="mb-3 row">
				<label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
					<?php echo form_error("email", "<small class='text-danger'>", "</small>"); ?>
				</div>
			</div>
			<div class="mb-3 row">
				<label for="staticEmail" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password" placeholder="Password">
					<?php echo form_error("password", "<small class='text-danger'>", "</small>"); ?>
				</div>
			</div>
			<div class="mb-3 row">
				<label for="staticEmail" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password2" placeholder="Confirm Password">
					<?php echo form_error("password2", "<small class='text-danger'>", "</small>"); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn btn-primary" style="float: right;" type="submit">Register</button>
					<a href="<?php echo base_url(); ?>login">sudah punya akun?</a>
				</div>
			</div>
			<!-- <input type="submit" class="btn btn-block btn-lg bg-pink waves-effect" value="Register">
		</button> -->

			<!-- <div class="m-t-25 m-b--5 align-center">
				<a href="<?php echo base_url(); ?>login">You already have a membership?</a>
			</div> -->
		</form>
	</div>
</div>
