<div class="container mt-5">
	<div class="body">
		<form id="sign_in" method="POST" action="<?php echo base_url(); ?>login/doLogin">
			<div class="mb-4">Sign in to access application</div>
			<?php echo $this->session->flashdata('msg'); ?>
			<div class="mb-3 row">
				<label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" name="email" id="inputPassword">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password" id="inputPassword">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn btn-primary" style="float: right;" type="submit">SIGN IN</button>
					<a href="<?php echo base_url(); ?>login/daftar">Register Now!</a>
				</div>
			</div>
		</form>
	</div>
</div>
