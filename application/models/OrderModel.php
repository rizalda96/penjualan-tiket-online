<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class OrderModel extends CI_Model {

	function insertData($data)
	{
		$this->db->insert('order', $data);
	}

	function listOrder()
	{
		// $this->db->query("SELECT
		// 	a.tanggal,
		// 	a.harga,
		// 	a.seat,
		// 	tujuan.nama_kota,
		// 	asal.nama_kota
		// FROM
		// 	`order` a
		// INNER JOIN kotas tujuan ON tujuan.id = a.tujuan
		// INNER JOIN kota asal ON asal.id = a.asal")->result();

		$sql= "SELECT
			a.tanggal,
			a.harga,
			a.seat,
			tujuan.nama_kota as tujuan,
			asal.nama_kota as asal
		FROM
			`order` a
		INNER JOIN kota tujuan ON tujuan.id = a.tujuan
		INNER JOIN kota asal ON asal.id = a.asal";
	//echo $sql;die;
		$result=$this->db->query($sql)->result();

		return $result;

		// ret
		// echo '<pre>';
		// print_r($result);die;
	}
}
