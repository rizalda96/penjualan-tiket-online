<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class KotaModel extends CI_Model {

	var $column_search = array('nama_kota');


	public function _get_datatables_query()
	{

		$this->db->from('kota');

		// $i = 0;

		// foreach ($this->column_search as $item)
		// {
		// 	if ($_POST['search']['value'])
		// 	{
		// 		$this->db->like('LOWER(' . $item . ')', strtolower($_POST['search']['value']));
		// 	}
		// 	$i++;
		// }
	}

	public function count_filtered($search)
	{
		$this->_get_datatables_query();
		if ($search){
			$this->db->like("nama_kota", $search);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from('kota');
		return $this->db->count_all_results();
	}

	public function get_datatables($search, $length, $start)
	{
		// $this->_get_datatables_query();
		$this->db->from('kota');
		
		if ($search) {
			$this->db->like("nama_kota", $search);
		}

		if ($length != -1) $this->db->limit($length, $start);
		$query = $this->db->get();
		return $query->result();
	}

	function deleteData($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('kota');
	}

	function insertData($data)
	{
		$this->db->insert('kota', $data);
	}

	function dataById($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('kota')->row();
	}

	function updateData($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('kota', $data);
	}
}
