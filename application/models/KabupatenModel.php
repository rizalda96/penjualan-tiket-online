<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KabupatenModel extends CI_Model
{

	var $column_search = array('nama_kabupaten');

	public function count_filtered($search)
	{
		$this->db->from('kabupaten');
		if ($search) {
			$this->db->like("nama_kabupaten", $search);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from('kabupaten');
		return $this->db->count_all_results();
	}

	public function get_datatables($search, $length, $start)
	{
		$this->db->from('kabupaten');

		if ($search) {
			$this->db->like("nama_kabupaten", $search);
		}

		if ($length != -1) $this->db->limit($length, $start);
		$query = $this->db->get();
		return $query->result();
	}

	function deleteData($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('kabupaten');
	}

	function insertData($data)
	{
		$this->db->insert('kabupaten', $data);
	}

	function dataById($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('kabupaten')->row();
	}

	function updateData($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('kabupaten', $data);
	}
}
