<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kota extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
		$this->load->model('KotaModel'); 
	}

	function index()
	{
    $this->load->view('template/header');
    $this->load->view('template/content/kota/index');
    $this->load->view('template/footer');
	}

	public function list()
	{
		$start = $_POST['start'];
		$draw = $_POST['draw'];
		$length = $_POST['length'];
		$search = $_POST['search']['value'];
		$orderBy = $_POST[0]['dir'];

		$list = $this->KotaModel->get_datatables($search, $length, $start);
		// $data = [];
		// $no = $start;
		// foreach ($list as $item) {
		// 	$no++;
		// 	$row = array();
		// 	$row[] = $no;
		// 	$row[] = $item->nama_kota;
		// 	$row[] = $item->status_aktif;
		// 	$btnedit 	= "	<button class='btn btn-primary btn-xs btn_edit' data-id= ". $item->id .">
    //         					Edit
    //         				</button> &nbsp;";
		// 	$btndelete 	= "	<button class='btn btn-danger btn-xs btn_hapus' data-id= " . $item->id . ">
    //         					Hapus
    //         				</button> &nbsp;";
		// 	$btn 		= "" . $btnedit . $btndelete . "";
		// 	$row[] = $btn;
		// 	$data[] = $row;
		// }

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->KotaModel->count_all(),
			"recordsFiltered" => $this->KotaModel->count_filtered($search),
			"data" => $list,
		);

		echo json_encode($output);
	}

	public function listKota()
	{
		$query = $this->db->get('kota')->result_array();

		echo json_encode($query);
	}

	function tambahData()
	{
		$nama_kota = $this->input->post('nama_kota');
		$status_aktif = $this->input->post('status_aktif');

		$data = [
			'nama_kota' => $nama_kota, 
			'status_aktif' => $status_aktif
		];
		$data = $this->KotaModel->insertData($data);
		echo json_encode($data); 
	}

	function getDataById()
	{
		$id = $this->input->post('id');
		$data = $this->KotaModel->dataById($id); 
		echo json_encode($data);
	}

	function perbaruiData()
	{
		$id = $this->input->post('id');
		$nama_kota = $this->input->post('nama_kota_edit');
		$status_aktif = $this->input->post('status_aktif');

		$data = [
			'nama_kota' => $nama_kota,
			'status_aktif' => $status_aktif
		];

		$result = $this->KotaModel->updateData($id, $data);

		echo json_encode($result);
	}

	function hapusData()
	{
		$id = $this->input->post('id');
		$data = $this->KotaModel->deleteData($id);
		echo json_encode($data);
	}
}
