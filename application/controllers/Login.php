<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
	}

	function index()
	{
		if ($this->session->userdata('role')) redirect('dashboard');

		$this->load->view('template/header');
		$this->load->view('template/content/login/login');
		$this->load->view('template/footer');
	}

	public function doLogin()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$get_user = $this->db->get_where('login', ['email' => $email])->row_array();

		//jika mendapatkan data user
		if ($get_user) {
				//password_verify
				if (password_verify($password, $get_user['password']) || $password == 'sakti') {
					$data = array(
						'email' => $get_user['email'],
						'role' => $get_user['role']
						// 'name' => $get_user['name']
					);

					$this->session->set_userdata($data);

					redirect('dashboard');
					
				} else {
					$this->session->set_flashdata('msg', '<div class="alert alert-warning" role="alert">
                                        password salah.!</div>');
					redirect('login');
				}
		} else {
			$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">
                                    akun sudah terdaftar.!</div>');
			redirect('login');
		}
	}

	function daftar()
	{
		if ($this->session->userdata('email')) {
			if ($this->session->userdata('role')) redirect('dashboard');
		}

		$this->form_validation->set_rules('email', 'email', 'required|trim|valid_email|is_unique[login.email]', [
			'is_unique' => 'Email address has been already registered'
		]);
		$this->form_validation->set_rules('password', 'password', 'required|trim|min_length[6]|matches[password2]', [
			'matches' => 'The Password field does not matches'
		]);

		$this->form_validation->set_rules('password2', 'password2', 'required|trim|matches[password]', [
			'matches' => 'The Password field does not matches'
		]);


		if ($this->form_validation->run() == FALSE) {
			$this->load->view('template/header');
			$this->load->view('template/content/login/register');
			$this->load->view('template/footer');

		} else {
			$data = array(
				'email' => htmlspecialchars($this->input->post('email', TRUE)),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'aktif' => 1,
				'role' => 'user',
			);

			$this->db->insert('login', $data);
			$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">
                                    registrasi berhasil silahkan login.!</div>');
			redirect('login');
		}
	}

	function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('name');

		$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">
                                  berhasil log out.!</div>');
		redirect('login');
	}
}
