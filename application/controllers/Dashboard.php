<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
	}

	function index()
	{
		if ($this->session->userdata('role') == 'admin') {
			$this->load->view('template/header');
			$this->load->view('template/content/home');
			$this->load->view('template/footer');
		} else {
			$this->load->view('template/header');
			$this->load->view('template/content/home');
			$this->load->view('template/footer');
		}

	}
}
