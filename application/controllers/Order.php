<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
		$this->load->model('OrderModel'); 
	}

	function index()
	{
    if ($this->session->userdata('role') == 'admin') {
			$this->load->view('template/header');
			$this->load->view('template/content/order/indexAdmin');
			$this->load->view('template/footer');
    } else {
			$this->load->view('template/header');
			$this->load->view('template/content/order/indexUser');
			$this->load->view('template/footer');
		}
	}

	function bayar()
	{
		$asal = $this->input->post('asal');
		$tujuan = $this->input->post('tujuan');
		$tanggal = $this->input->post('tanggal');
		$seat = $this->input->post('seat');
		$harga = $this->input->post('harga');

		$data = [
			'asal' => $asal,
			'tujuan' => $tujuan,
			'tanggal' => $tanggal,
			'harga' => $harga,
			'seat' => $seat,
		];

		$data = $this->OrderModel->insertData($data);
		echo json_encode($data);
	}

	public function list()
	{
		$query = $this->OrderModel->listOrder();
		echo json_encode($query);
	}
}
