<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
		$this->load->model('KabupatenModel');
	}

	function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/content/kabupaten/index');
		$this->load->view('template/footer');
	}

	public function list()
	{
		$start = $_POST['start'];
		$draw = $_POST['draw'];
		$length = $_POST['length'];
		$search = $_POST['search']['value'];

		$list = $this->KabupatenModel->get_datatables($search, $length, $start);

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $this->KabupatenModel->count_all(),
			"recordsFiltered" => $this->KabupatenModel->count_filtered($search),
			"data" => $list,
		);

		echo json_encode($output);
	}

	public function listKab()
	{
		$query = $this->db->get('kabupaten')->result_array();

		echo json_encode($query);
	}

	function tambahData()
	{
		$nama_kabupaten = $this->input->post('nama_kab');
		$status_aktif = $this->input->post('status_aktif');
		// var_dump($nama_kabupaten); die;
		$data = [
			'nama_kabupaten' => $nama_kabupaten,
			'status_aktif' => $status_aktif
		];
		$data = $this->KabupatenModel->insertData($data);
		echo json_encode($data);
	}

	function getDataById()
	{
		$id = $this->input->post('id');
		$data = $this->KabupatenModel->dataById($id);
		echo json_encode($data);
	}

	function perbaruiData()
	{
		$id = $this->input->post('id');
		$nama_kabupaten_edit = $this->input->post('nama_kab_edit');
		$status_aktif = $this->input->post('status_aktif');

		$data = [
			'nama_kabupaten' => $nama_kabupaten_edit,
			'status_aktif' => $status_aktif
		];

		$result = $this->KabupatenModel->updateData($id, $data);

		echo json_encode($result);
	}

	function hapusData()
	{
		$id = $this->input->post('id');
		$data = $this->KabupatenModel->deleteData($id);
		echo json_encode($data);
	}
}
