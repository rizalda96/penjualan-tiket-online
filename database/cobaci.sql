/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : cobaci

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 24/12/2021 17:15:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kota
-- ----------------------------
DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status_aktif` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kota
-- ----------------------------
INSERT INTO `kota` VALUES (1, 'cimahi', 1);
INSERT INTO `kota` VALUES (4, 'bandung', 1);
INSERT INTO `kota` VALUES (6, 'jakarta', 1);
INSERT INTO `kota` VALUES (12, 'bogor', 1);

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `aktif` int(11) NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES (1, 'admin@admin.com', 'admin', 1, 'admin');
INSERT INTO `login` VALUES (2, 'cek@gmail.com', '$2y$10$i2kvUoLq6LGfJzJdsWwecugLy5nGYYtnxD9dvAnroFjeSOj/R21Iy', 1, 'user');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asal` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tujuan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal` datetime NULL DEFAULT NULL,
  `harga` double NULL DEFAULT NULL,
  `seat` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1, NULL, NULL, '2021-12-23 00:00:00', NULL, NULL);
INSERT INTO `order` VALUES (2, '12', '4', '2021-12-30 00:00:00', NULL, NULL);
INSERT INTO `order` VALUES (3, '4', '4', '2021-12-30 00:00:00', NULL, NULL);
INSERT INTO `order` VALUES (4, '', '', '2021-12-31 00:00:00', NULL, NULL);
INSERT INTO `order` VALUES (5, '4', '1', '2021-12-23 00:00:00', NULL, NULL);
INSERT INTO `order` VALUES (6, '', '', '0000-00-00 00:00:00', NULL, NULL);
INSERT INTO `order` VALUES (7, '', '', '0000-00-00 00:00:00', NULL, 4);
INSERT INTO `order` VALUES (8, '4', '6', '2021-12-29 00:00:00', NULL, 4);
INSERT INTO `order` VALUES (9, '1', '4', '2021-12-30 00:00:00', NULL, 2);
INSERT INTO `order` VALUES (10, '', '4', '0000-00-00 00:00:00', NULL, 2);
INSERT INTO `order` VALUES (11, '', '4', '0000-00-00 00:00:00', NULL, 2);
INSERT INTO `order` VALUES (12, '1', '4', '2021-12-30 00:00:00', 50000, 5);
INSERT INTO `order` VALUES (13, '4', '6', '2021-12-30 00:00:00', 60000, 5);

SET FOREIGN_KEY_CHECKS = 1;
